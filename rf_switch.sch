EESchema Schematic File Version 4
LIBS:camdds-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4200 2800 0    50   Input ~ 0
RF_IN
Text HLabel 6400 2800 2    50   Output ~ 0
RF_OUT
Wire Wire Line
	6000 2800 5900 2800
$Comp
L Device:R R?
U 1 1 5C59D90F
P 6000 3250
F 0 "R?" H 5930 3204 50  0000 R CNN
F 1 "50" H 5930 3295 50  0000 R CNN
F 2 "" V 5930 3250 50  0001 C CNN
F 3 "~" H 6000 3250 50  0001 C CNN
	1    6000 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6000 3100 6000 3000
Wire Wire Line
	6000 3000 5900 3000
Wire Wire Line
	6000 3500 6000 3400
Wire Wire Line
	5300 3500 5300 3400
Wire Wire Line
	4700 2800 4600 2800
Text HLabel 1500 4750 0    50   Input ~ 0
Enable
Text HLabel 3100 5600 2    50   Output ~ 0
Output_Enabled
$Comp
L power:GNDA #PWR?
U 1 1 5C59F1D1
P 6000 3500
F 0 "#PWR?" H 6000 3250 50  0001 C CNN
F 1 "GNDA" H 6005 3327 50  0000 C CNN
F 2 "" H 6000 3500 50  0001 C CNN
F 3 "" H 6000 3500 50  0001 C CNN
	1    6000 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR?
U 1 1 5C59F1F9
P 5300 3500
F 0 "#PWR?" H 5300 3250 50  0001 C CNN
F 1 "GNDA" H 5305 3327 50  0000 C CNN
F 2 "" H 5300 3500 50  0001 C CNN
F 3 "" H 5300 3500 50  0001 C CNN
	1    5300 3500
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4049 U?
U 3 1 5CBF3BA4
P 2000 4750
F 0 "U?" H 2000 5067 50  0000 C CNN
F 1 "CD74HC4049M" H 2000 4976 50  0000 C CNN
F 2 "" H 2000 4750 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4049ubms.pdf" H 2000 4750 50  0001 C CNN
	3    2000 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 4750 1600 4750
Connection ~ 1600 4750
Wire Wire Line
	1600 4750 1700 4750
$Comp
L Device:C C?
U 1 1 5CBF47E6
P 6150 2800
F 0 "C?" V 5898 2800 50  0000 C CNN
F 1 "C" V 5989 2800 50  0000 C CNN
F 2 "" H 6188 2650 50  0001 C CNN
F 3 "~" H 6150 2800 50  0001 C CNN
	1    6150 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 2800 6300 2800
$Comp
L Device:C C?
U 1 1 5CBF4937
P 4450 2800
F 0 "C?" V 4198 2800 50  0000 C CNN
F 1 "C" V 4289 2800 50  0000 C CNN
F 2 "" H 4488 2650 50  0001 C CNN
F 3 "~" H 4450 2800 50  0001 C CNN
	1    4450 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 2800 4200 2800
Text Notes 4500 2450 0    50   ~ 0
TODO: make symbol for RSW-2-25-PA+
$Comp
L 4xxx:4049 U?
U 4 1 5CBF5135
P 2050 5600
F 0 "U?" H 2050 5917 50  0000 C CNN
F 1 "CD74HC4049M" H 2050 5826 50  0000 C CNN
F 2 "" H 2050 5600 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4049ubms.pdf" H 2050 5600 50  0001 C CNN
	4    2050 5600
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4049 U?
U 5 1 5CBF523E
P 2700 5600
F 0 "U?" H 2700 5917 50  0000 C CNN
F 1 "CD74HC4049M" H 2700 5826 50  0000 C CNN
F 2 "" H 2700 5600 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4049ubms.pdf" H 2700 5600 50  0001 C CNN
	5    2700 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5600 2350 5600
Wire Wire Line
	3100 5600 3000 5600
Wire Wire Line
	1750 5600 1600 5600
$Comp
L RF_Switch:MSWA-2-20 U?
U 1 1 5C59D816
P 5300 3000
F 0 "U?" H 5300 3467 50  0000 C CNN
F 1 "MSWA-2-20" H 5300 3376 50  0000 C CNN
F 2 "Package_SO:SOP-8_4.14x5.33mm_P1.27mm" H 5300 3450 50  0001 C CNN
F 3 "https://ww2.minicircuits.com/pdfs/MSWA-2-20+.pdf" H 4750 4250 50  0001 C CNN
	1    5300 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 4750 1600 5000
Wire Wire Line
	4700 3200 4600 3200
Wire Wire Line
	4600 3200 4600 5000
Wire Wire Line
	4600 5000 1600 5000
Connection ~ 1600 5000
Wire Wire Line
	1600 5000 1600 5600
Wire Wire Line
	2300 4750 4500 4750
Wire Wire Line
	4500 4750 4500 3100
Wire Wire Line
	4500 3100 4700 3100
$EndSCHEMATC
