EESchema Schematic File Version 4
LIBS:camdds-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1550 2900 0    50   Input ~ 0
RF_IN
Text HLabel 8600 2800 2    50   Output ~ 0
RF_OUT
Text HLabel 1500 4750 0    50   Input ~ 0
Enable
Text HLabel 3100 5600 2    50   Output ~ 0
Output_Enabled
$Comp
L 4xxx:4049 U?
U 3 1 5CBF3BA4
P 2000 4750
F 0 "U?" H 2000 5067 50  0000 C CNN
F 1 "CD74HC4049M" H 2000 4976 50  0000 C CNN
F 2 "" H 2000 4750 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4049ubms.pdf" H 2000 4750 50  0001 C CNN
	3    2000 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 4750 1600 4750
Connection ~ 1600 4750
Wire Wire Line
	1600 4750 1700 4750
Wire Wire Line
	8600 2800 6300 2800
Text Notes 4500 2450 0    50   ~ 0
TODO: make symbol for RSW-2-25-PA+
$Comp
L 4xxx:4049 U?
U 4 1 5CBF5135
P 2050 5600
F 0 "U?" H 2050 5917 50  0000 C CNN
F 1 "CD74HC4049M" H 2050 5826 50  0000 C CNN
F 2 "" H 2050 5600 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4049ubms.pdf" H 2050 5600 50  0001 C CNN
	4    2050 5600
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4049 U?
U 5 1 5CBF523E
P 2700 5600
F 0 "U?" H 2700 5917 50  0000 C CNN
F 1 "CD74HC4049M" H 2700 5826 50  0000 C CNN
F 2 "" H 2700 5600 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4049ubms.pdf" H 2700 5600 50  0001 C CNN
	5    2700 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5600 2350 5600
Wire Wire Line
	3100 5600 3000 5600
Wire Wire Line
	1750 5600 1600 5600
Wire Wire Line
	1600 4750 1600 5000
Wire Wire Line
	4600 5000 1600 5000
Connection ~ 1600 5000
Wire Wire Line
	1600 5000 1600 5600
Wire Wire Line
	2300 4750 4500 4750
$Comp
L Transformer:ADT1-1WT TR?
U 1 1 5CBFC6DD
P 2850 3000
AR Path="/5BD46B93/5CBFC6DD" Ref="TR?"  Part="1" 
AR Path="/5C566D91/5CBFC6DD" Ref="TR?"  Part="1" 
F 0 "TR?" H 2850 3378 50  0000 C CNN
F 1 "ADT1-1WT" H 2850 3287 50  0000 C CNN
F 2 "Package_SO:Mini-Circuits_CD542_H2.84mm" H 2850 2650 50  0001 C CNN
F 3 "https://ww2.minicircuits.com/pdfs/ADT1-1WT+.pdf" H 2850 3000 50  0001 C CNN
	1    2850 3000
	-1   0    0    1   
$EndComp
Wire Wire Line
	2650 2800 1850 2800
Wire Wire Line
	1550 2900 2150 2900
Wire Wire Line
	2150 2900 2150 3200
Wire Wire Line
	2150 3200 2650 3200
$Comp
L power:GND #PWR?
U 1 1 5CBFC6E8
P 2550 3000
AR Path="/5BD46B93/5CBFC6E8" Ref="#PWR?"  Part="1" 
AR Path="/5C566D91/5CBFC6E8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2550 2750 50  0001 C CNN
F 1 "GND" V 2555 2872 50  0000 R CNN
F 2 "" H 2550 3000 50  0001 C CNN
F 3 "" H 2550 3000 50  0001 C CNN
	1    2550 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	2550 3000 2650 3000
$Comp
L Device:R R?
U 1 1 5CBFC6EF
P 2150 3450
AR Path="/5BD46B93/5CBFC6EF" Ref="R?"  Part="1" 
AR Path="/5C566D91/5CBFC6EF" Ref="R?"  Part="1" 
F 0 "R?" H 2220 3496 50  0000 L CNN
F 1 "50" H 2220 3405 50  0000 L CNN
F 2 "" V 2080 3450 50  0001 C CNN
F 3 "~" H 2150 3450 50  0001 C CNN
	1    2150 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5CBFC6F6
P 1850 3450
AR Path="/5BD46B93/5CBFC6F6" Ref="R?"  Part="1" 
AR Path="/5C566D91/5CBFC6F6" Ref="R?"  Part="1" 
F 0 "R?" H 1920 3496 50  0000 L CNN
F 1 "50" H 1920 3405 50  0000 L CNN
F 2 "" V 1780 3450 50  0001 C CNN
F 3 "~" H 1850 3450 50  0001 C CNN
	1    1850 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3300 2150 3200
Connection ~ 2150 3200
Wire Wire Line
	1850 3300 1850 2800
Connection ~ 1850 2800
Wire Wire Line
	1850 2800 1550 2800
Wire Wire Line
	1850 3700 1850 3600
Wire Wire Line
	2150 3700 2150 3600
Text HLabel 1550 2800 0    50   Input ~ 0
~RF_IN
$Comp
L power:GNDA #PWR?
U 1 1 5CC031F6
P 3150 3350
F 0 "#PWR?" H 3150 3100 50  0001 C CNN
F 1 "GNDA" H 3155 3177 50  0000 C CNN
F 2 "" H 3150 3350 50  0001 C CNN
F 3 "" H 3150 3350 50  0001 C CNN
	1    3150 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3350 3150 3200
Wire Wire Line
	3150 3200 3050 3200
$Comp
L power:GNDA #PWR?
U 1 1 5CC0380C
P 2150 3700
F 0 "#PWR?" H 2150 3450 50  0001 C CNN
F 1 "GNDA" H 2155 3527 50  0000 C CNN
F 2 "" H 2150 3700 50  0001 C CNN
F 3 "" H 2150 3700 50  0001 C CNN
	1    2150 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR?
U 1 1 5CC0382B
P 1850 3700
F 0 "#PWR?" H 1850 3450 50  0001 C CNN
F 1 "GNDA" H 1855 3527 50  0000 C CNN
F 2 "" H 1850 3700 50  0001 C CNN
F 3 "" H 1850 3700 50  0001 C CNN
	1    1850 3700
	1    0    0    -1  
$EndComp
Text Notes 3400 2800 0    50   ~ 0
TODO: LFCN-400
Text Notes 6900 2700 0    50   ~ 0
TODO: ERA-5XSM?
$EndSCHEMATC
