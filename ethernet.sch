EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R19
U 1 1 5C472F8C
P 2000 2400
F 0 "R19" H 1930 2354 50  0000 R CNN
F 1 "2k2" H 1930 2445 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1930 2400 50  0001 C CNN
F 3 "~" H 2000 2400 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "TNPW06032K20BEEA" H 0   0   50  0001 C CNN "MPN"
F 6 "Digi-Key" H 0   0   50  0001 C CNN "SPR"
F 7 "541-2012-1-ND" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    2000 2400
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C472F92
P 2000 2700
F 0 "#PWR?" H 2000 2450 50  0001 C CNN
F 1 "GND" H 2005 2527 50  0000 C CNN
F 2 "" H 2000 2700 50  0001 C CNN
F 3 "" H 2000 2700 50  0001 C CNN
	1    2000 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R34
U 1 1 5C47326D
P 7700 4350
F 0 "R34" H 7630 4304 50  0000 R CNN
F 1 "2k2" H 7630 4395 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7630 4350 50  0001 C CNN
F 3 "~" H 7700 4350 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "TNPW06032K20BEEA" H 0   0   50  0001 C CNN "MPN"
F 6 "Digi-Key" H 0   0   50  0001 C CNN "SPR"
F 7 "541-2012-1-ND" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    7700 4350
	-1   0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5C4733B8
P 7700 4650
F 0 "#PWR?" H 7700 4500 50  0001 C CNN
F 1 "+3V3" H 7715 4823 50  0000 C CNN
F 2 "" H 7700 4650 50  0001 C CNN
F 3 "" H 7700 4650 50  0001 C CNN
	1    7700 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R33
U 1 1 5C473B57
P 7250 4350
F 0 "R33" H 7180 4304 50  0000 R CNN
F 1 "4k87 1%" H 7180 4395 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7180 4350 50  0001 C CNN
F 3 "~" H 7250 4350 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "CRCW06034K87FKEA" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "679-0481P" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    7250 4350
	-1   0    0    1   
$EndComp
$Comp
L Device:C C66
U 1 1 5C473D53
P 6550 4450
F 0 "C66" H 6665 4496 50  0000 L CNN
F 1 "100n" H 6665 4405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6588 4300 50  0001 C CNN
F 3 "~" H 6550 4450 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "163-7298" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6550 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C64
U 1 1 5C4740BB
P 6250 4650
F 0 "C64" H 6365 4696 50  0000 L CNN
F 1 "100n" H 6365 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6288 4500 50  0001 C CNN
F 3 "~" H 6250 4650 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "163-7298" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C63
U 1 1 5C47411C
P 5950 4800
F 0 "C63" H 6065 4846 50  0000 L CNN
F 1 "100n" H 6065 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5988 4650 50  0001 C CNN
F 3 "~" H 5950 4800 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "163-7298" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    5950 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C47518B
P 5950 5150
F 0 "#PWR?" H 5950 4900 50  0001 C CNN
F 1 "GND" H 5955 4977 50  0000 C CNN
F 2 "" H 5950 5150 50  0001 C CNN
F 3 "" H 5950 5150 50  0001 C CNN
	1    5950 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C47616E
P 7250 4650
F 0 "#PWR?" H 7250 4400 50  0001 C CNN
F 1 "GND" H 7255 4477 50  0000 C CNN
F 2 "" H 7250 4650 50  0001 C CNN
F 3 "" H 7250 4650 50  0001 C CNN
	1    7250 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C4769A9
P 4700 5000
F 0 "#PWR?" H 4700 4750 50  0001 C CNN
F 1 "GND" H 4705 4827 50  0000 C CNN
F 2 "" H 4700 5000 50  0001 C CNN
F 3 "" H 4700 5000 50  0001 C CNN
	1    4700 5000
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5C4D2F10
P 4700 800
F 0 "#PWR?" H 4700 650 50  0001 C CNN
F 1 "+3V3" H 4715 973 50  0000 C CNN
F 2 "" H 4700 800 50  0001 C CNN
F 3 "" H 4700 800 50  0001 C CNN
	1    4700 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:C C60
U 1 1 5C4D71F6
P 4900 1150
F 0 "C60" H 5015 1196 50  0000 L CNN
F 1 "100n" H 5015 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4938 1000 50  0001 C CNN
F 3 "~" H 4900 1150 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "163-7298" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    4900 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C61
U 1 1 5C4D85BF
P 5250 1150
F 0 "C61" H 5365 1196 50  0000 L CNN
F 1 "100n" H 5365 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5288 1000 50  0001 C CNN
F 3 "~" H 5250 1150 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "163-7298" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    5250 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C62
U 1 1 5C4D85F5
P 5600 1150
F 0 "C62" H 5715 1196 50  0000 L CNN
F 1 "100n" H 5715 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5638 1000 50  0001 C CNN
F 3 "~" H 5600 1150 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "163-7298" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    5600 1150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C4DF913
P 5700 1400
F 0 "#PWR?" H 5700 1150 50  0001 C CNN
F 1 "GND" V 5705 1272 50  0000 R CNN
F 2 "" H 5700 1400 50  0001 C CNN
F 3 "" H 5700 1400 50  0001 C CNN
	1    5700 1400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C6F136B
P 8650 3500
F 0 "#PWR?" H 8650 3250 50  0001 C CNN
F 1 "GND" H 8655 3327 50  0000 C CNN
F 2 "" H 8650 3500 50  0001 C CNN
F 3 "" H 8650 3500 50  0001 C CNN
	1    8650 3500
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5C6F5326
P 9000 1750
F 0 "#PWR?" H 9000 1600 50  0001 C CNN
F 1 "+3V3" H 9015 1923 50  0000 C CNN
F 2 "" H 9000 1750 50  0001 C CNN
F 3 "" H 9000 1750 50  0001 C CNN
	1    9000 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C702D59
P 9700 2900
F 0 "#PWR?" H 9700 2650 50  0001 C CNN
F 1 "GND" H 9705 2727 50  0000 C CNN
F 2 "" H 9700 2900 50  0001 C CNN
F 3 "" H 9700 2900 50  0001 C CNN
	1    9700 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R29
U 1 1 5C708774
P 6200 3400
F 0 "R29" V 5993 3400 50  0000 C CNN
F 1 "270" V 6084 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6130 3400 50  0001 C CNN
F 3 "~" H 6200 3400 50  0001 C CNN
F 4 "TE" H 0   0   50  0001 C CNN "MFR"
F 5 "CRGCQ0603F270R" H 0   0   50  0001 C CNN "MPN"
F 6 "Digi-Key" H 0   0   50  0001 C CNN "SPR"
F 7 "A129681CT-ND" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6200 3400
	0    1    1    0   
$EndComp
$Comp
L Device:R R30
U 1 1 5C7105B9
P 6450 3600
F 0 "R30" V 6243 3600 50  0000 C CNN
F 1 "270" V 6334 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6380 3600 50  0001 C CNN
F 3 "~" H 6450 3600 50  0001 C CNN
F 4 "TE" H 0   0   50  0001 C CNN "MFR"
F 5 "CRGCQ0603F270R" H 0   0   50  0001 C CNN "MPN"
F 6 "Digi-Key" H 0   0   50  0001 C CNN "SPR"
F 7 "A129681CT-ND" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6450 3600
	0    1    1    0   
$EndComp
$Comp
L Device:R R27
U 1 1 5C720B55
P 6050 1800
F 0 "R27" V 5843 1800 50  0000 C CNN
F 1 "50" V 5934 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5980 1800 50  0001 C CNN
F 3 "~" H 6050 1800 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "CRCW060349R9FKEA" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "679-0459P" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6050 1800
	0    1    1    0   
$EndComp
$Comp
L Device:R R31
U 1 1 5C720BE6
P 6950 1800
F 0 "R31" V 6743 1800 50  0000 C CNN
F 1 "50" V 6834 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6880 1800 50  0001 C CNN
F 3 "~" H 6950 1800 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "CRCW060349R9FKEA" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "679-0459P" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6950 1800
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5C72DB34
P 6300 1300
F 0 "#PWR?" H 6300 1150 50  0001 C CNN
F 1 "+3V3" H 6315 1473 50  0000 C CNN
F 2 "" H 6300 1300 50  0001 C CNN
F 3 "" H 6300 1300 50  0001 C CNN
	1    6300 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C65
U 1 1 5C73EA1C
P 6550 1550
F 0 "C65" H 6665 1596 50  0000 L CNN
F 1 "100n" H 6665 1505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6588 1400 50  0001 C CNN
F 3 "~" H 6550 1550 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "163-7298" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6550 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C74460C
P 6550 1300
F 0 "#PWR?" H 6550 1050 50  0001 C CNN
F 1 "GND" H 6555 1127 50  0000 C CNN
F 2 "" H 6550 1300 50  0001 C CNN
F 3 "" H 6550 1300 50  0001 C CNN
	1    6550 1300
	-1   0    0    1   
$EndComp
$Comp
L Device:R R28
U 1 1 5C74D8CC
P 6050 2500
F 0 "R28" V 5843 2500 50  0000 C CNN
F 1 "50" V 5934 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5980 2500 50  0001 C CNN
F 3 "~" H 6050 2500 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "CRCW060349R9FKEA" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "679-0459P" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6050 2500
	0    1    1    0   
$EndComp
$Comp
L Device:R R32
U 1 1 5C74D8D2
P 6950 2500
F 0 "R32" V 6743 2500 50  0000 C CNN
F 1 "50" V 6834 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6880 2500 50  0001 C CNN
F 3 "~" H 6950 2500 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "CRCW060349R9FKEA" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "679-0459P" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6950 2500
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5C74D8DC
P 6500 2400
F 0 "#PWR?" H 6500 2250 50  0001 C CNN
F 1 "+3V3" H 6515 2573 50  0000 C CNN
F 2 "" H 6500 2400 50  0001 C CNN
F 3 "" H 6500 2400 50  0001 C CNN
	1    6500 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C67
U 1 1 5C74D8E5
P 6900 3150
F 0 "C67" V 6648 3150 50  0000 C CNN
F 1 "100n" V 6739 3150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6938 3000 50  0001 C CNN
F 3 "~" H 6900 3150 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "163-7298" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6900 3150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C74D8EE
P 7150 3150
F 0 "#PWR?" H 7150 2900 50  0001 C CNN
F 1 "GND" V 7155 3022 50  0000 R CNN
F 2 "" H 7150 3150 50  0001 C CNN
F 3 "" H 7150 3150 50  0001 C CNN
	1    7150 3150
	0    -1   -1   0   
$EndComp
$Comp
L Oscillator:ASE-xxxMHz X1
U 1 1 5C77865E
P 1700 4850
F 0 "X1" H 1850 5100 50  0000 L CNN
F 1 "ASEM1-50.000MHz-LC" H 1800 4550 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_Abracon_ASE-4Pin_3.2x2.5mm" H 2400 4500 50  0001 C CNN
F 3 "http://www.abracon.com/Oscillators/ASV.pdf" H 1600 4850 50  0001 C CNN
F 4 "Abracon" H -600 0   50  0001 C CNN "MFR"
F 5 "ASEM1-50.000MHz-LC-T" H -600 0   50  0001 C CNN "MPN"
F 6 "RS Components" H -600 0   50  0001 C CNN "SPR"
F 7 "171-2770P" H -600 0   50  0001 C CNN "SPN"
F 8 "-" H -600 0   50  0001 C CNN "SPURL"
	1    1700 4850
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5C77CEF1
P 1700 4450
F 0 "#PWR?" H 1700 4300 50  0001 C CNN
F 1 "+3V3" H 1715 4623 50  0000 C CNN
F 2 "" H 1700 4450 50  0001 C CNN
F 3 "" H 1700 4450 50  0001 C CNN
	1    1700 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C780E19
P 1700 5250
F 0 "#PWR?" H 1700 5000 50  0001 C CNN
F 1 "GND" H 1705 5077 50  0000 C CNN
F 2 "" H 1700 5250 50  0001 C CNN
F 3 "" H 1700 5250 50  0001 C CNN
	1    1700 5250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R26
U 1 1 5C7A4D97
P 3350 3200
F 0 "R26" V 3143 3200 50  0000 C CNN
F 1 "2k2" V 3234 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3280 3200 50  0001 C CNN
F 3 "~" H 3350 3200 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "TNPW06032K20BEEA" H 0   0   50  0001 C CNN "MPN"
F 6 "Digi-Key" H 0   0   50  0001 C CNN "SPR"
F 7 "541-2012-1-ND" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    3350 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R21
U 1 1 5C7C3FD1
P 2450 4300
F 0 "R21" V 2243 4300 50  0000 C CNN
F 1 "50" V 2334 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2380 4300 50  0001 C CNN
F 3 "~" H 2450 4300 50  0001 C CNN
F 4 "Vishay" H -600 0   50  0001 C CNN "MFR"
F 5 "CRCW060349R9FKEA" H -600 0   50  0001 C CNN "MPN"
F 6 "RS" H -600 0   50  0001 C CNN "SPR"
F 7 "679-0459P" H -600 0   50  0001 C CNN "SPN"
F 8 "-" H -600 0   50  0001 C CNN "SPURL"
	1    2450 4300
	0    1    1    0   
$EndComp
$Comp
L Device:C C59
U 1 1 5C7CE4DD
P 1900 6500
F 0 "C59" H 2015 6546 50  0000 L CNN
F 1 "100n" H 2015 6455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1938 6350 50  0001 C CNN
F 3 "~" H 1900 6500 50  0001 C CNN
F 4 "Wurth Elektronik" H 200 850 50  0001 C CNN "MFR"
F 5 "885012206095" H 200 850 50  0001 C CNN "MPN"
F 6 "RS" H 200 850 50  0001 C CNN "SPR"
F 7 "163-7298" H 200 850 50  0001 C CNN "SPN"
F 8 "-" H 200 850 50  0001 C CNN "SPURL"
	1    1900 6500
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5C7CE9BB
P 1900 6250
F 0 "#PWR?" H 1900 6100 50  0001 C CNN
F 1 "+3V3" H 1915 6423 50  0000 C CNN
F 2 "" H 1900 6250 50  0001 C CNN
F 3 "" H 1900 6250 50  0001 C CNN
	1    1900 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C7D3892
P 1900 6750
F 0 "#PWR?" H 1900 6500 50  0001 C CNN
F 1 "GND" H 1905 6577 50  0000 C CNN
F 2 "" H 1900 6750 50  0001 C CNN
F 3 "" H 1900 6750 50  0001 C CNN
	1    1900 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C70
U 1 1 5C6A725D
P 8650 3100
F 0 "C70" H 8765 3146 50  0000 L CNN
F 1 "100n" H 8765 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8688 2950 50  0001 C CNN
F 3 "~" H 8650 3100 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "163-7298" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    8650 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C69
U 1 1 5C6A7579
P 8250 2800
F 0 "C69" H 8365 2846 50  0000 L CNN
F 1 "100n" H 8365 2755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8288 2650 50  0001 C CNN
F 3 "~" H 8250 2800 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   -300 50  0001 C CNN "MFR"
F 5 "885012206095" H 0   -300 50  0001 C CNN "MPN"
F 6 "RS" H 0   -300 50  0001 C CNN "SPR"
F 7 "163-7298" H 0   -300 50  0001 C CNN "SPN"
F 8 "-" H 0   -300 50  0001 C CNN "SPURL"
	1    8250 2800
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP37
U 1 1 5CA1175E
P 2700 4200
F 0 "TP37" H 2758 4320 50  0000 L CNN
F 1 "50MHz" H 2758 4229 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2900 4200 50  0001 C CNN
F 3 "~" H 2900 4200 50  0001 C CNN
F 4 "-" H -600 0   50  0001 C CNN "MFR"
F 5 "-" H -600 0   50  0001 C CNN "MPN"
F 6 "-" H -600 0   50  0001 C CNN "SPR"
F 7 "-" H -600 0   50  0001 C CNN "SPN"
F 8 "-" H -600 0   50  0001 C CNN "SPURL"
	1    2700 4200
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5D6A928D
P 3050 3200
F 0 "#PWR?" H 3050 3050 50  0001 C CNN
F 1 "+3V3" V 3065 3328 50  0000 L CNN
F 2 "" H 3050 3200 50  0001 C CNN
F 3 "" H 3050 3200 50  0001 C CNN
	1    3050 3200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R20
U 1 1 5D6CAB78
P 2200 3450
F 0 "R20" H 2270 3496 50  0000 L CNN
F 1 "1k5" H 2270 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2130 3450 50  0001 C CNN
F 3 "~" H 2200 3450 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "CRCW06031K50FKEAHP" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "820-6789P" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    2200 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5D6D1328
P 2200 3200
F 0 "#PWR?" H 2200 3050 50  0001 C CNN
F 1 "+3V3" H 2215 3373 50  0000 C CNN
F 2 "" H 2200 3200 50  0001 C CNN
F 3 "" H 2200 3200 50  0001 C CNN
	1    2200 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R35
U 1 1 5D8776BD
P 8100 4350
F 0 "R35" H 8030 4304 50  0000 R CNN
F 1 "2k2" H 8030 4395 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8030 4350 50  0001 C CNN
F 3 "~" H 8100 4350 50  0001 C CNN
F 4 "Vishay" H 400 0   50  0001 C CNN "MFR"
F 5 "TNPW06032K20BEEA" H 400 0   50  0001 C CNN "MPN"
F 6 "Digi-Key" H 400 0   50  0001 C CNN "SPR"
F 7 "541-2012-1-ND" H 400 0   50  0001 C CNN "SPN"
F 8 "-" H 400 0   50  0001 C CNN "SPURL"
	1    8100 4350
	-1   0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5D87E163
P 8100 4650
F 0 "#PWR?" H 8100 4500 50  0001 C CNN
F 1 "+3V3" H 8115 4823 50  0000 C CNN
F 2 "" H 8100 4650 50  0001 C CNN
F 3 "" H 8100 4650 50  0001 C CNN
	1    8100 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:C C68
U 1 1 5D8A2417
P 6900 4450
F 0 "C68" H 7015 4496 50  0000 L CNN
F 1 "10u" H 7015 4405 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 6938 4300 50  0001 C CNN
F 3 "~" H 6900 4450 50  0001 C CNN
F 4 "TDK" H 0   0   50  0001 C CNN "MFR"
F 5 "C3225X7R1E106M250AC" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "916-3021P" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6900 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R23
U 1 1 5D9990F5
P 3300 2600
F 0 "R23" V 3200 2600 50  0000 C CNN
F 1 "50" V 3300 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3230 2600 50  0001 C CNN
F 3 "~" H 3300 2600 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "CRCW060349R9FKEA" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "679-0459P" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    3300 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R R24
U 1 1 5D99972E
P 3300 2700
F 0 "R24" V 3200 2700 50  0000 C CNN
F 1 "50" V 3300 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3230 2700 50  0001 C CNN
F 3 "~" H 3300 2700 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "CRCW060349R9FKEA" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "679-0459P" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    3300 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R25
U 1 1 5D9C10DD
P 3300 3500
F 0 "R25" V 3200 3500 50  0000 C CNN
F 1 "50" V 3300 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3230 3500 50  0001 C CNN
F 3 "~" H 3300 3500 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "CRCW060349R9FKEA" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "679-0459P" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    3300 3500
	0    1    1    0   
$EndComp
$Comp
L Interface_Ethernet:DP83848C U13
U 1 1 5C472951
P 4700 3200
F 0 "U13" H 4700 4981 50  0000 C CNN
F 1 "DP83848C" H 4700 4890 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 5700 1650 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/dp83848c.pdf" H 4700 3200 50  0001 C CNN
F 4 "TI" H 0   0   50  0001 C CNN "MFR"
F 5 "DP83848IVV/NOPB" H 0   0   50  0001 C CNN "MPN"
F 6 "RS Components" H 0   0   50  0001 C CNN "SPR"
F 7 "651-5766P" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    4700 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R22
U 1 1 5D9AA6E6
P 3250 4300
F 0 "R22" V 3043 4300 50  0000 C CNN
F 1 "50" V 3134 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3180 4300 50  0001 C CNN
F 3 "~" H 3250 4300 50  0001 C CNN
F 4 "Vishay" H 200 0   50  0001 C CNN "MFR"
F 5 "CRCW060349R9FKEA" H 200 0   50  0001 C CNN "MPN"
F 6 "RS" H 200 0   50  0001 C CNN "SPR"
F 7 "679-0459P" H 200 0   50  0001 C CNN "SPN"
F 8 "-" H 200 0   50  0001 C CNN "SPURL"
	1    3250 4300
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5D9FE148
P 9600 1300
F 0 "#PWR?" H 9600 1150 50  0001 C CNN
F 1 "+3V3" H 9615 1473 50  0000 C CNN
F 2 "" H 9600 1300 50  0001 C CNN
F 3 "" H 9600 1300 50  0001 C CNN
	1    9600 1300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5D9FE516
P 9900 1300
F 0 "#PWR?" H 9900 1150 50  0001 C CNN
F 1 "+3V3" H 9915 1473 50  0000 C CNN
F 2 "" H 9900 1300 50  0001 C CNN
F 3 "" H 9900 1300 50  0001 C CNN
	1    9900 1300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Pulse_JXD6-0001NL J6
U 1 1 5D99A10F
P 9700 2200
F 0 "J6" H 9170 2246 50  0000 R CNN
F 1 "Pulse_JXD6-0001NL" H 9170 2155 50  0000 R CNN
F 2 "Connector_RJ:RJ45_Pulse_JXD6-0001NL_Horizontal" H 9700 1450 50  0001 C CNN
F 3 "https://productfinder.pulseeng.com/doc_type/WEB301/doc_num/JXD6-0001NL/doc_part/JXD6-0001NL.pdf" H 9285 1965 50  0001 L TNN
F 4 "Pulse" H 0   0   50  0001 C CNN "MFR"
F 5 "JXD6-0001NL" H 0   0   50  0001 C CNN "MPN"
F 6 "Digi-Key" H 0   0   50  0001 C CNN "SPR"
F 7 "553-3744-ND" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    9700 2200
	-1   0    0    -1  
$EndComp
Text HLabel 3450 1900 0    50   Input ~ 0
TXD_0
Text HLabel 3450 2000 0    50   Input ~ 0
TXD_1
Text HLabel 3450 2400 0    50   Input ~ 0
TX_EN
Text HLabel 2900 2600 0    50   Output ~ 0
RXD_0
Text HLabel 2900 2700 0    50   Output ~ 0
RXD_1
Text HLabel 950  3900 0    50   Input ~ 0
~RESET
Text HLabel 2200 4300 0    50   Output ~ 0
CLK
Text HLabel 2900 3500 0    50   Output ~ 0
CRS_DV
Text HLabel 3450 3600 0    50   Input ~ 0
MDC
Text HLabel 1750 3700 0    50   BiDi ~ 0
MDIO
Text Label 7450 1900 0    50   ~ 0
TD+
Text Label 7450 2100 0    50   ~ 0
TD-
Text Label 7450 2300 0    50   ~ 0
RD+
Text Label 7450 2500 0    50   ~ 0
RD-
Text HLabel 1300 4850 0    50   Input ~ 0
XTAL_ENABLE
Wire Wire Line
	2000 2550 2000 2700
Wire Wire Line
	2000 2250 2000 2200
Wire Wire Line
	2000 2100 3700 2100
Wire Wire Line
	3700 2200 2000 2200
Wire Wire Line
	2000 2200 2000 2100
Wire Wire Line
	7700 4500 7700 4650
Wire Wire Line
	7700 4200 7700 3900
Wire Wire Line
	5700 4200 5800 4200
Wire Wire Line
	5800 4200 5800 4400
Wire Wire Line
	5800 4400 5700 4400
Wire Wire Line
	5800 4400 5800 4500
Wire Wire Line
	5800 4500 5700 4500
Wire Wire Line
	5700 3900 7700 3900
Wire Wire Line
	7250 4200 7250 4000
Wire Wire Line
	7250 4000 5700 4000
Wire Wire Line
	5800 4500 5950 4500
Wire Wire Line
	5950 4500 5950 4650
Wire Wire Line
	6250 4500 6250 4400
Wire Wire Line
	6250 4400 5800 4400
Wire Wire Line
	5800 4200 6550 4200
Wire Wire Line
	6550 4200 6550 4300
Wire Wire Line
	6900 4300 6900 4200
Wire Wire Line
	6900 4200 6550 4200
Wire Wire Line
	5950 5150 5950 5050
Wire Wire Line
	5950 5050 6250 5050
Wire Wire Line
	6250 5050 6250 4800
Wire Wire Line
	5950 5050 5950 4950
Wire Wire Line
	6250 5050 6550 5050
Wire Wire Line
	6550 5050 6550 4600
Wire Wire Line
	6550 5050 6900 5050
Wire Wire Line
	6900 5050 6900 4600
Wire Wire Line
	7250 4650 7250 4500
Wire Wire Line
	4700 5000 4700 4900
Wire Wire Line
	4900 4800 4900 4900
Wire Wire Line
	4900 4900 4800 4900
Wire Wire Line
	4700 4900 4700 4800
Wire Wire Line
	4800 4900 4800 4800
Wire Wire Line
	4800 4900 4700 4900
Wire Wire Line
	4700 4900 4600 4900
Wire Wire Line
	4600 4900 4600 4800
Wire Wire Line
	4600 4900 4500 4900
Wire Wire Line
	4500 4900 4500 4800
Wire Wire Line
	3700 2400 3450 2400
Wire Wire Line
	3450 2000 3700 2000
Wire Wire Line
	3700 1900 3450 1900
Wire Wire Line
	3700 3700 2200 3700
Wire Wire Line
	3450 3600 3700 3600
Wire Wire Line
	4700 800  4700 900 
Wire Wire Line
	4700 1500 4800 1500
Wire Wire Line
	4800 1500 4800 1600
Wire Wire Line
	4700 1500 4700 1600
Wire Wire Line
	4700 1500 4600 1500
Wire Wire Line
	4600 1500 4600 1600
Wire Wire Line
	4900 1000 4900 900 
Wire Wire Line
	4900 900  4700 900 
Wire Wire Line
	4700 900  4700 1500
Wire Wire Line
	4900 900  5250 900 
Wire Wire Line
	5250 900  5250 1000
Wire Wire Line
	5250 900  5600 900 
Wire Wire Line
	5600 900  5600 1000
Wire Wire Line
	4900 1300 4900 1400
Wire Wire Line
	4900 1400 5250 1400
Wire Wire Line
	5600 1300 5600 1400
Wire Wire Line
	5600 1400 5700 1400
Wire Wire Line
	5250 1300 5250 1400
Wire Wire Line
	5250 1400 5600 1400
Wire Wire Line
	9000 1750 9000 2000
Wire Wire Line
	9000 2000 9100 2000
Wire Wire Line
	9100 2400 9000 2400
Wire Wire Line
	9000 2400 9000 2000
Wire Wire Line
	6050 3400 5700 3400
Wire Wire Line
	6350 3400 8000 3400
Wire Wire Line
	8100 3600 6600 3600
Wire Wire Line
	6300 3600 5700 3600
Wire Wire Line
	5700 1900 5800 1900
Wire Wire Line
	5900 1800 5800 1800
Wire Wire Line
	5800 1800 5800 1900
Wire Wire Line
	5800 1900 9100 1900
Wire Wire Line
	6200 1800 6300 1800
Wire Wire Line
	7100 1800 7200 1800
Wire Wire Line
	7200 1800 7200 2100
Wire Wire Line
	6300 1300 6300 1800
Wire Wire Line
	6300 1800 6550 1800
Wire Wire Line
	6550 1700 6550 1800
Wire Wire Line
	6550 1800 6800 1800
Wire Wire Line
	6550 1400 6550 1300
Wire Wire Line
	6200 2500 6500 2500
Wire Wire Line
	6500 2400 6500 2500
Wire Wire Line
	5700 2600 5800 2600
Wire Wire Line
	7300 2600 7300 2300
Wire Wire Line
	7300 2300 9100 2300
Wire Wire Line
	9100 2500 7400 2500
Wire Wire Line
	5700 2800 7400 2800
Wire Wire Line
	7100 2500 7400 2500
Wire Wire Line
	7400 2500 7400 2800
Wire Wire Line
	5900 2500 5800 2500
Wire Wire Line
	5800 2500 5800 2600
Wire Wire Line
	5800 2600 7300 2600
Wire Wire Line
	7150 3150 7050 3150
Wire Wire Line
	5700 2100 7200 2100
Wire Wire Line
	6500 2500 6800 2500
Wire Wire Line
	6500 2500 6500 3150
Wire Wire Line
	6500 3150 6750 3150
Wire Wire Line
	1700 4450 1700 4550
Wire Wire Line
	1700 5250 1700 5150
Wire Wire Line
	3700 3200 3500 3200
Wire Wire Line
	3200 3200 3050 3200
Wire Wire Line
	2000 4850 2700 4850
Wire Wire Line
	2700 4850 2700 4300
Wire Wire Line
	2300 4300 2200 4300
Wire Wire Line
	2700 4300 2600 4300
Wire Wire Line
	1900 6250 1900 6350
Wire Wire Line
	1900 6750 1900 6650
Wire Wire Line
	9000 2400 8650 2400
Wire Wire Line
	8650 2400 8650 2950
Wire Wire Line
	2700 4200 2700 4300
Wire Wire Line
	2200 3600 2200 3700
Wire Wire Line
	2200 3700 1750 3700
Wire Wire Line
	2200 3200 2200 3300
Wire Wire Line
	8100 3800 8100 4200
Wire Wire Line
	5700 3800 8100 3800
Wire Wire Line
	8100 4650 8100 4500
Wire Wire Line
	3450 2600 3700 2600
Wire Wire Line
	3450 2700 3700 2700
Wire Wire Line
	2900 2600 3150 2600
Wire Wire Line
	2900 2700 3150 2700
Wire Wire Line
	3450 3500 3700 3500
Wire Wire Line
	2900 3500 3150 3500
Wire Wire Line
	950  3900 3700 3900
Wire Wire Line
	3400 4300 3700 4300
Wire Wire Line
	3100 4300 2700 4300
Wire Wire Line
	1400 4850 1300 4850
Wire Wire Line
	7200 2100 9100 2100
Wire Wire Line
	8000 1400 9500 1400
Wire Wire Line
	9500 1400 9500 1600
Wire Wire Line
	8000 1400 8000 3400
Wire Wire Line
	9800 1600 9800 1500
Wire Wire Line
	9800 1500 8100 1500
Wire Wire Line
	8100 1500 8100 3600
Wire Wire Line
	8650 3250 8650 3350
Wire Wire Line
	9000 2000 8250 2000
Wire Wire Line
	8250 2000 8250 2650
Wire Wire Line
	8250 2950 8250 3350
Wire Wire Line
	8250 3350 8650 3350
Wire Wire Line
	8650 3350 8650 3500
Wire Wire Line
	9600 1300 9600 1600
Wire Wire Line
	9900 1300 9900 1600
Wire Wire Line
	9700 2900 9700 2800
Connection ~ 2000 2200
Connection ~ 5800 4400
Connection ~ 5800 4500
Connection ~ 5800 4200
Connection ~ 6550 4200
Connection ~ 5950 5050
Connection ~ 6250 5050
Connection ~ 6550 5050
Connection ~ 4700 4900
Connection ~ 4800 4900
Connection ~ 4600 4900
Connection ~ 4700 1500
Connection ~ 4700 900 
Connection ~ 4900 900 
Connection ~ 5250 900 
Connection ~ 5600 1400
Connection ~ 5250 1400
Connection ~ 9000 2000
Connection ~ 5800 1900
Connection ~ 7200 2100
Connection ~ 6300 1800
Connection ~ 6550 1800
Connection ~ 6500 2500
Connection ~ 7400 2500
Connection ~ 5800 2600
Connection ~ 2700 4300
Connection ~ 9000 2400
Connection ~ 2200 3700
Connection ~ 8650 3350
NoConn ~ 3700 4500
NoConn ~ 3700 2800
NoConn ~ 3700 2900
NoConn ~ 3700 3000
NoConn ~ 3700 2300
NoConn ~ 3700 4000
NoConn ~ 5700 3500
NoConn ~ 3700 4200
NoConn ~ 3700 3400
NoConn ~ 3700 3100
$EndSCHEMATC
